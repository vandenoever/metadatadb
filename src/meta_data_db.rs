use notify::op;
use libsqlite3_sys::Error;
use libsqlite3_sys::ErrorCode::ConstraintViolation;
use notify::{RecommendedWatcher, Watcher};
use notify;
use result::Result;
use rusqlite::Connection;
use rusqlite::types::Value;
use rusqlite::Error::SqliteFailure;
use rusqlite;
use std::clone::Clone;
use std::fs;
use std::os::unix::fs::MetadataExt;
use std::path::{Path, PathBuf};
use std::sync::mpsc::{Receiver, Sender, channel, sync_channel};
use std::sync::{Arc, Mutex};
use std::thread;
use std::time::{Duration, SystemTime};
use walkdir::WalkDir;

pub struct MetaDataDb {
    pub conn: Arc<Mutex<rusqlite::Connection>>,
}

impl MetaDataDb {
    /// create a new MetaDataDb from a path.
    /// The path should point to no file or to a file created by
    /// MetaDataDb
    pub fn open<P: AsRef<Path>>(db_path: P, root_path: String) -> Result<Self> {
        let r = fs::metadata(&db_path);
        match r {
            Ok(ref f) if f.file_type().is_file() => open_existing(db_path, root_path),
            _ => open_new(db_path, root_path),
        }
    }
    pub fn search(&self,
                  query: String,
                  sort_column: i64,
                  sort_order: i64,
                  results: &mut Vec<SearchResult>) {
        do_search(query, sort_column, sort_order, results, self);
    }
}
fn set_pragmas(conn: &Connection) -> Result<()> {
    try!(conn.execute_batch("
        PRAGMA encoding = 'UTF-8';
        PRAGMA page_size = 8192;
        PRAGMA locking_mode = EXCLUSIVE;
        PRAGMA synchronous = OFF;
        PRAGMA temp_store = MEMORY;
        PRAGMA journal_mode = OFF;
        PRAGMA busy_timeout = 10000;
        PRAGMA foreign_keys = ON;"));
    Ok(())
}
fn open_new<P: AsRef<Path>>(db_path: P, root_path: String) -> Result<MetaDataDb> {
    fs::remove_file(&db_path).err();
    let conn = try!(Connection::open(db_path));
    try!(set_pragmas(&conn));
    try!(conn.execute_batch("
        CREATE TABLE dir (
             dirid    INTEGER PRIMARY KEY NOT NULL,
             dir      TEXT UNIQUE NOT NULL
        );
        CREATE TABLE IF NOT EXISTS entry (
             docid    INTEGER PRIMARY KEY NOT NULL,
             dir      INTEGER NOT NULL,
             size     INTEGER NOT NULL,
             mtime    INTEGER NOT NULL,
             hash     BLOB,
             FOREIGN KEY(dir) REFERENCES dir(dirid)
        );
        CREATE VIRTUAL TABLE filename USING fts4(name);
        "));
    watch(conn, root_path, fill_database)
}
fn open_existing<P: AsRef<Path>>(db_path: P, root_path: String) -> Result<MetaDataDb> {
    let conn = try!(Connection::open(db_path));
    try!(set_pragmas(&conn));
    watch(conn, root_path, update_database)
}

fn watch(conn: rusqlite::Connection,
         root_path: String,
         f: fn(&Arc<Mutex<rusqlite::Connection>>, String))
         -> Result<MetaDataDb> {
    let conn = Arc::new(Mutex::new(conn));
    let thread_conn = conn.clone();
    thread::spawn(move || if let Err(err) = watch_thread(thread_conn, root_path.clone(), f) {
        println!("oops {}", err);
    });
    Ok(MetaDataDb { conn: conn })
}
fn watch_thread(conn: Arc<Mutex<rusqlite::Connection>>,
                root_path: String,
                f: fn(&Arc<Mutex<rusqlite::Connection>>, String))
                -> Result<()> {
    let (tx, rx) = channel();
    let mut watcher: RecommendedWatcher = try!(Watcher::new(tx));

    // install watcher before doing the comparison with the database
    // to detect files created during that comparison
    match watcher.watch(root_path.clone()) {
        Ok(_) => {}
        Err(e) => println!("error setting directory watch: {}", e),
    }

    f(&conn, root_path);

    let tx = update_thread(conn);
    watch_loop(rx, tx, &mut watcher)
}

pub struct Update {
    path: PathBuf,
    time: SystemTime,
    update_type: UpdateType,
}

enum UpdateType {
    Change,
    Remove,
}

fn update_thread(conn: Arc<Mutex<rusqlite::Connection>>) -> Sender<Update> {
    let (tx, rx) = channel();
    thread::spawn(move || if let Err(err) = update_loop(conn, rx) {
        println!("{}", err);
    });
    tx
}

fn update(update_type: UpdateType, path: PathBuf) -> Update {
    Update {
        path: path,
        time: SystemTime::now(),
        update_type: update_type,
    }
}

fn update_loop(mut conn: Arc<Mutex<rusqlite::Connection>>, rx: Receiver<Update>) -> Result<()> {
    let mut stack = Vec::new();
    let mut last_update_time = SystemTime::now();
    let update_interval = Duration::new(1, 0); // 1 second
    loop {
        let r = rx.recv();
        if let Err(e) = r {
            println!("{}", e);
            continue;
        } else {
            let r = r.unwrap();
            stack.push(r);
        }
        let now = SystemTime::now();
        if let Ok(interval) = now.duration_since(last_update_time) {
            if interval > update_interval {
                apply_updates(&stack, &mut conn);
                last_update_time = now;
            }
        }
    }
}

fn apply_updates(stack: &Vec<Update>, conn: &mut Arc<Mutex<rusqlite::Connection>>) {
    if let Err(_) = conn.lock() {
        for update in stack {
            match update.update_type {
                UpdateType::Remove => {
                    println!("{} {:?}", &update.path.display(), update.time);
                }
                _ => {
                    println!("{} {:?}", &update.path.display(), update.time);
                }
            }
        }
    }
}

fn watch_loop(rx: Receiver<notify::Event>,
              tx: Sender<Update>,
              watcher: &mut RecommendedWatcher)
              -> Result<()> {
    loop {
        // possible operations: CHMOD, CREATE, IGNORED, REMOVE, RENAME, WRITE
        match rx.recv() {
            Ok(notify::Event { path: Some(path), op: Ok(op::CHMOD) }) => {
                try!(tx.send(update(UpdateType::Change, path)));
            }
            Ok(notify::Event { path: Some(path), op: Ok(op::CREATE) }) => {
                println!("{:?} {:?}", op::CREATE, path);
                let metadata = fs::metadata(&path);
                if let Ok(metadata) = metadata {
                    if metadata.is_dir() {
                        if let Err(e) = watcher.watch(&path) {
                            println!("error adding watch on {:?} {}", path, e);
                        }
                    } else {
                        try!(tx.send(update(UpdateType::Change, path)));
                        //add_file_to_db(&mut conn, &path);
                        // remove_dir_from_db(&mut conn, &path);
                        // remove_file_from_db(&mut conn, &path);
                    }
                } else {
                    println!("Cannot read metadata on {:?}", path);
                }
            }
            Ok(notify::Event { path: Some(path), op: Ok(op::REMOVE) }) => {
                try!(tx.send(update(UpdateType::Remove, path)));
            }
            Ok(notify::Event { path: Some(path), op: Ok(op::RENAME) }) => {
                try!(tx.send(update(UpdateType::Remove, path)));
            }
            Ok(notify::Event { path: Some(path), op: Ok(op::WRITE) }) => {
                try!(tx.send(update(UpdateType::Change, path)));
            }
            Ok(notify::Event { path: Some(path), op: Ok(op) }) => {
                println!("{:?} {:?}", op, path);
            }
            Err(e) => println!("watch error {}", e),
            _ => (),
        }
    }
}

fn write_db(rx: Receiver<WalkEntry>, conn: &mut rusqlite::Connection) -> rusqlite::Result<()> {
    // todo: have one prev_dir and dir_id for each depth
    let mut insert_dir = conn.prepare("INSERT INTO dir (dir)
                                 VALUES ($1)")
        .unwrap();
    let mut select_dir = conn.prepare("SELECT dirid FROM dir WHERE dir = $1")
        .unwrap();
    let mut stmt1 = conn.prepare("INSERT INTO filename (name)
                                 VALUES ($1)")
        .unwrap();
    let mut stmt2 = conn.prepare("INSERT INTO entry (dir, size, mtime)
                                 VALUES \
                  ($1, $2, $3)")
        .unwrap();
    let mut prev_dir = String::from("MAGIC_VALUE");
    let mut dir_id: i64 = 0;
    while let Ok(e) = rx.recv() {
        if e.dir != prev_dir {
            match insert_dir.execute(&[&e.dir]) {
                Err(SqliteFailure(Error { code: ConstraintViolation, .. }, _)) => {
                    dir_id = select_dir.query(&[&e.dir])
                        .unwrap()
                        .next()
                        .unwrap()
                        .unwrap()
                        .get(0);
                }
                _ => {
                    dir_id = conn.last_insert_rowid();
                }
            }
            prev_dir = e.dir;
        }
        let mtime = e.metadata.mtime();
        let size = e.metadata.len() as i64;
        try!(stmt1.execute(&[&e.file_name]));
        try!(stmt2.execute(&[&dir_id, &size, &mtime]));
    }
    try!(conn.execute("INSERT INTO filename(name) VALUES('optimize');", &[]));
    Ok(())
}

fn fill_database(conn: &Arc<Mutex<rusqlite::Connection>>, root_path: String) {
    let mut conn = conn.lock().unwrap();
    if let Err(e) = write_db(walk(root_path), &mut conn) {
        println!("{}", e);
    }
    if let Err(e) = create_indices(&mut conn) {
        println!("{}", e);
    }
}

fn update_database(conn: &Arc<Mutex<rusqlite::Connection>>, root_path: String) {
    if let Err(_) = conn.lock() {
        println!("update_database: {}", root_path);
    }
}

fn create_indices(conn: &mut rusqlite::Connection) -> rusqlite::Result<()> {
    try!(conn.execute("CREATE INDEX IF NOT EXISTS entry_size ON entry (size)", &[]));
    try!(conn.execute("CREATE INDEX IF NOT EXISTS entry_mtime ON entry (mtime)",
                      &[]));
    Ok(())
}

struct WalkEntry {
    dir: String,
    file_name: String,
    metadata: fs::Metadata,
}

fn walk(path: String) -> Receiver<WalkEntry> {
    // Create a channel to receive the events.
    let (tx, rx) = sync_channel::<WalkEntry>(10000);
    thread::spawn(move || {
        for entry in WalkDir::new(path) {
            // sort(true)
            let entry = entry.unwrap();
            let metadata = entry.metadata().unwrap();
            if !metadata.is_file() {
                continue;
            }
            let path = entry.path();
            let file_entry = WalkEntry {
                dir: String::from(path.parent().unwrap().to_str().unwrap()),
                file_name: String::from(path.file_name().unwrap().to_str().unwrap()),
                metadata: metadata,
            };
            if let Err(e) = tx.send(file_entry) {
                println!("{}", e);
                break;
            }
        }
    });
    rx
}

pub struct SearchResult {
    pub filename: String,
    pub size: i64,
    pub mtime: i64,
    pub dir: String,
}

fn to_string(value: Value) -> String {
    match value {
        Value::Text(string) => string,
        Value::Blob(bytes) => {
            match String::from_utf8(bytes) {
                Ok(string) => string,
                Err(_) => String::from("<path is not utf8>"),
            }
        }
        _ => String::from(""),
    }
}

fn to_int(value: Value) -> i64 {
    match value {
        Value::Integer(v) => v,
        _ => -1,
    }
}

fn get_sorting(sort_column: i64, sort_order: i64) -> String {
    let column = match sort_column {
        0 => "f.name",
        1 => "e.size",
        2 => "e.mtime",
        _ => "e.dir",
    };
    let order = match sort_order {
        0 => "ASC",
        _ => "DESC",
    };
    format!("{} {}", column, order)
}

fn do_search(query: String,
             sort_column: i64,
             sort_order: i64,
             results: &mut Vec<SearchResult>,
             mdb: &MetaDataDb) {
    let sql = format!("SELECT d.dir, f.name, e.size, e.mtime
         FROM filename AS f
         \
                       JOIN entry AS e USING(docid)
         JOIN dir AS d ON e.dir = d.dirid
         \
                       WHERE f.name match ? ORDER BY {} LIMIT 10000",
                      get_sorting(sort_column, sort_order));
    let conn = mdb.conn.lock().unwrap();
    let mut stmt = conn.prepare(&sql).unwrap();
    results.clear();
    let iter = stmt.query_map(&[&query], |row| {
            SearchResult {
                dir: to_string(row.get(0)),
                filename: to_string(row.get(1)),
                size: to_int(row.get(2)),
                mtime: to_int(row.get(3)),
            }
        })
        .unwrap();
    for path in iter {
        results.push(path.unwrap());
    }
    println!("Found {} hits.", results.len());
}
