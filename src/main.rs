extern crate libsqlite3_sys;
extern crate notify;
#[macro_use]
extern crate qml;
extern crate rusqlite;
extern crate walkdir;

mod meta_data_db;
mod result;

use result::Result;
use meta_data_db::{MetaDataDb, SearchResult};
use std::env;

use qml::*;

Q_LISTMODEL!{
    pub SearcherModel {
        filename: String,
        size: f64,
        mtime: i32,
        dir: String
    }
}

pub struct Searcher {
    mdb: MetaDataDb,
    query: String,
    sort_column: i32,
    sort_order: i32,
    results: Vec<SearchResult>,
    model: SearcherModel,
}

impl Searcher {
    fn search(&mut self, query: String, sort_column: i32, sort_order: i32) -> Option<&QVariant> {
        if !(self.query == query && self.sort_column == sort_column &&
             self.sort_order == sort_order) {
            self.query = query.clone();
            self.mdb.search(query,
                            sort_column as i64,
                            sort_order as i64,
                            &mut self.results);
            self.sort_order = sort_order;
            self.sort_column = sort_column;

            let r = self.results
                .iter()
                .map(|r| (r.filename.clone(), r.size as f64, r.mtime as i32, r.dir.clone()))
                .collect::<Vec<_>>();
            self.model.set_data(r);
        }
        None
    }
}

Q_OBJECT! (

pub Searcher as QSearcher {
signals:
     fn notify_new_result();
slots:
     fn search(a: String, b: i32, c: i32);
properties:
});

fn ui(mdb: MetaDataDb) -> Result<()> {

    let list = SearcherModel::new();
    let qvar: QVariant = list.get_qvar();

    let searcher = Searcher {
        mdb: mdb,
        query: String::new(),
        sort_column: 0,
        sort_order: 0,
        results: Vec::new(),
        model: list,
    };

    let mut engine = QmlEngine::new();
    let qsearcher = QSearcher::new(searcher);
    engine.set_property("results", &qvar);
    engine.set_and_store_property("searcher", qsearcher.get_qobj());
    engine.load_file("ui/search_ui.qml");
    engine.exec();
    Ok(())
}

fn run(db_path: &str, search_path: &str) -> Result<()> {
    let mdb = try!(MetaDataDb::open(db_path, String::from(search_path)));
    ui(mdb)
}

fn main() {
    let mut args = env::args();
    let exe = args.next().unwrap();
    match (args.next(), args.next()) {
        (Some(db_path), Some(search_path)) => {
            let r = run(&db_path, &search_path);
            if let Err(e) = r {
                print!("Error: {}", e);
            }
        }
        _ => {
            print!("Usage: {} DATABASE DIR", exe);
        }
    }
}
