use rusqlite;
use notify;
use std::fmt;
use std::error;
use std::sync::mpsc::SendError;
use meta_data_db::Update;

#[derive(Debug)]
enum ErrorInner {
    Rusqlite { err: rusqlite::Error },
    Notify { err: notify::Error },
    Send { err: SendError<Update> },
}

#[derive(Debug)]
pub struct Error {
    err: ErrorInner,
}

impl error::Error for Error {
    fn description(&self) -> &str {
        match self.err {
            ErrorInner::Rusqlite { ref err } => err.description(),
            ErrorInner::Notify { ref err } => err.description(),
            ErrorInner::Send { ref err } => err.description(),
        }
    }
    fn cause(&self) -> Option<&error::Error> {
        match self.err {
            ErrorInner::Rusqlite { ref err } => Some(err),
            ErrorInner::Notify { ref err } => Some(err),
            ErrorInner::Send { ref err } => Some(err),
        }
    }
}

impl fmt::Display for Error {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        match self.err {
            ErrorInner::Rusqlite { ref err } => err.fmt(f),
            ErrorInner::Notify { ref err } => err.fmt(f),
            ErrorInner::Send { ref err } => err.fmt(f),
        }
    }
}

impl From<rusqlite::Error> for Error {
    fn from(err: rusqlite::Error) -> Error {
        Error { err: ErrorInner::Rusqlite { err: err } }
    }
}
impl From<notify::Error> for Error {
    fn from(err: notify::Error) -> Error {
        Error { err: ErrorInner::Notify { err: err } }
    }
}
impl From<SendError<Update>> for Error {
    fn from(err: SendError<Update>) -> Error {
        Error { err: ErrorInner::Send { err: err } }
    }
}

pub type Result<Update> = ::std::result::Result<Update, Error>;
