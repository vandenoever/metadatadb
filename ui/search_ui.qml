import QtQuick 2.6
import QtQuick.Controls 1.5
import QtQuick.Layouts 1.3

ApplicationWindow {
    visible: true
    title: "Search"

    property int margin: 3
    width: mainLayout.implicitWidth + 2 * margin
    height: mainLayout.implicitHeight + 2 * margin
    minimumWidth: 600
    minimumHeight: 600

    ColumnLayout {
        id: mainLayout
        anchors.fill: parent

        TextField {
            id: searchField
            placeholderText: "Search..."
            Layout.fillWidth: true
            focus: true
            onAccepted: { search(); }
            Keys.onReleased: { search(); }
        }

        TableView {
            id: table
            model: results
            Layout.fillHeight: true
            Layout.fillWidth: true
            sortIndicatorVisible: true
            onSortIndicatorColumnChanged: { search(); }
            onSortIndicatorOrderChanged: { search(); }

            TableViewColumn {
                role: "filename"
                title: "File Name"
            }
            TableViewColumn {
                role: "size"
                title: "Size"
            }
            TableViewColumn {
                role: "mtime"
                title: "Modified"
            }
            TableViewColumn {
                role: "dir"
                title: "Directory"
            }
        }
    }
    function search() {
        var query = searchField.text;
        if (query.indexOf('*') === -1) {
            query += '*';
        }
        searcher.model = results;
        searcher.search(query, table.sortIndicatorColumn,
                                table.sortIndicatorOrder);
        var i;
        for (i = 0; i < table.columnCount; ++i) {
            table.getColumn(i).elideMode = Text.ElideMiddle;
        }
        table.resizeColumnsToContents();
    }
}
